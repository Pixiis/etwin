import { $Ucs2String } from "kryo/lib/ucs2-string.js";

/**
 * An absolute URL.
 */
export type Url = string;

export const $Url = $Ucs2String;
