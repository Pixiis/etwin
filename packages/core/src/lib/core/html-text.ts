import { $Ucs2String } from "kryo/lib/ucs2-string.js";

/**
 * Raw HTML content.
 */
export type HtmlText = string;

export const $HtmlText = $Ucs2String;
