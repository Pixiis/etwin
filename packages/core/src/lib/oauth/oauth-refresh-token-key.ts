import { $Ucs2String, Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type OauthRefreshTokenKey = string;

export const $OauthRefreshTokenKey: Ucs2StringType = $Ucs2String;
