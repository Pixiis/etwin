import { $Ucs2String, Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type OauthClientSecret = string;

export const $OauthClientSecret: Ucs2StringType = $Ucs2String;
